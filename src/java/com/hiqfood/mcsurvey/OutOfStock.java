package com.hiqfood.mcsurvey;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author parichat
 */
public class OutOfStock {
    public int ID;
    public int UserID;
    public String StoreID;	
    public String ItemID;	
    public int Action;
    public Date ActionDate;	
    public Date ShipDate;
    public String ActionComment;	
    public Date PostingDate;
    public String Case1;	
    public String Case2;
    public String Comment;
    public int MailSent;
        
    public static List<OutOfStock> getData() {
        List<OutOfStock> os = null;
        try {
            Connection conn = DAL.getDBConnection();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from OutOfStock");
            os = new ArrayList<>();
            while (rs.next()) {
                OutOfStock o = new OutOfStock();
                o.ID = rs.getInt(1);
                o.UserID = rs.getInt(2);
                o.StoreID = rs.getString(3);
                o.ItemID = rs.getString(4);
                o.Action = rs.getInt(5);
                o.ActionDate = rs.getDate(6);
                o.ShipDate = rs.getDate(7);
                o.ActionComment = rs.getString(8);
                o.PostingDate = rs.getDate(9);
                o.Case1 = rs.getString(10);
                o.Case2 = rs.getString(11);
                o.Comment = rs.getString(12);
                o.MailSent = rs.getInt(13);
                os.add(o);
            }
        } catch (SQLException ex) {
            Logger.getLogger(OutOfStock.class.getName()).log(Level.SEVERE, null, ex);
        }
        return os;
    }
}