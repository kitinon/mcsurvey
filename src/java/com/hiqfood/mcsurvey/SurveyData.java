/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.hiqfood.mcsurvey;

import java.sql.Date;

/**
 *
 * @author parichat
 */
public class SurveyData {
    public int ID;
    public int UserID;
    public String StoreID;
    public String ItemID;
    public int Check1;
    public int Check2;
    public int Check3;
    public int Check4;
    public int Unit;
    public int Carton;
    public Float Price;
    public String Promotion;
    public Date StartDate;
    public Date EndDate;
    public String PromotionType;
    public String ErrorMsg;
    public String Comment;
    public Float DMS;
    public Float DateSync;                                
}
