/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.hiqfood.mcsurvey;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kitinon
 */
public class DAL {
    private static Connection conn = null;
  
    public static Connection getDBConnection() {
        if (conn == null) {
            try {
                String connectionUrl = "jdbc:jtds:sqlserver://labtest.hiqfood.com;TDS=7.0;databaseName=MCSurvey_New;charset=TIS-620";
                Class.forName("net.sourceforge.jtds.jdbc.Driver");
                conn = DriverManager.getConnection(connectionUrl, "sa", "hiqit");
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(DAL.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return conn;
    }
}
