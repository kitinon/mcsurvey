/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.hiqfood.mcsurvey;

import java.sql.Date;

/**
 *
 * @author parichat
 */
public class UserTimeRecord {
        public int ID;
	public int UserID;
	public Date Date;	
        public Date CheckIn;
        public Date CheckOut;
        public String StoreID;	
        public String Latitude;	
        public String Longtitude;
        public Date DateSync;
        public String Location;  
}
